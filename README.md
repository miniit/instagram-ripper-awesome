# About

Extended version of the original [Instagram-Ripper](https://gitlab.com/rubygems/instagram-ripper)

This version must be maintained as a private project and all the source code never be released to the public.

Sponsored by [Edwyn Chan](mailto:edwyn2@gmail.com)

# Warning

*Remember that you'll never sign any piece of code with your name!!!*