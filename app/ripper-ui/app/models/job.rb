class Job < ApplicationRecord

	has_many :profiles

	def percent
		(ripped_media.to_f / total_media.to_f) * 100
	end
end
