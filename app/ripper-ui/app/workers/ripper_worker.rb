class RipperWorker
  include Sidekiq::Worker
  def perform(job_id, type = "all", has_location = false, max = 0)
    job = Job.find(job_id)

    path = "tmp/downloads/"
    folder_name = job.created_at.strftime("%Y%m%d%H%M")
    folder_path = path + folder_name

    if File.directory?(folder_path)
      require 'fileutils'
      FileUtils.rm_rf(folder_path)
    else
      Dir.mkdir folder_path
    end

    batch_size = 10
    
    #If max is not set, fetch all profile media
   	max = job.total_media if max == 0

    cycles = (max / batch_size).to_i
    cycles = cycles + 1 if (max % batch_size) != 0

   	puts "*** Cycles: #{cycles}"

  	job.profiles.each do |profile|
  		puts "=====> #{profile.username} <====="
      puts "=====> FROM DATE: #{profile.from_date} <====="
      if profile.is_location?
        p = Instagram::Location.new profile.username.to_i
      else
  		  p = Instagram::Profile.new profile.username.to_s
      end
		  json_file = File.open("#{path + folder_name}/#{profile.username}.json","w")
		  json_hash = {
        pagination: { next_url: "", next_max_id: "" },
        meta: {code: 200},
        data: []
      }
		  data_arr = []
      offset = 1
      limit = batch_size

  		1.upto(cycles) do
    		puts "* OFFSET: #{offset} | LIMIT: #{limit}"
        # Fetch in batch
        if type == "all"
          p.fetch_media offset..limit, { has_location: has_location, from: profile.from_date }
        elsif type == "photos"
          p.fetch_photos offset..limit, { has_location: has_location, from: profile.from_date }
        elsif type == "videos"
          p.fetch_videos offset..limit, { has_location: has_location, from: profile.from_date }
        end
          
  			ripped = job.ripped_media + batch_size
  			job.update_attribute(:ripped_media, ripped) # Update ripped data
  			data_arr = data_arr + p.extract_data # Extract data from batch ripped
  			offset = limit + 1
  			limit += batch_size
  		end

  		data_arr.each {|data| json_hash[:data] << data } # Input data into json hash
  		  json_file.write(json_hash.to_json) # Write to file json output
      	profile.update_attribute(:ripped, true) # Update Job status
      	json_file.close
      	
      	p.close_connection! # Close connection with webdriver
      	puts "========================="
      end
      job.update_attribute(:done, true)
    end
end