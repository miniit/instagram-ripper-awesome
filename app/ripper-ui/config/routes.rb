Rails.application.routes.draw do

  resources :profiles
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  root to: 'jobs#index'
  resources :jobs
  get 'jobs/status/:id' => 'jobs#status'
  get 'jobs/download/:id' => 'jobs#download'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
