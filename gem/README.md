# Instagram::Ripper

A simple ruby gem to load (or reap :)) a entire page from an Instagram profile. It supports Instagram authentication with Instagram credentials only (user and password).

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'instagram-ripper'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install instagram-ripper

## Usage

```ruby
require 'instagram/ripper'

# If you need to authenticate
Instagram::Ripper.authenticate 'myusername', 'mypassword'

# It will reap the entire page of https://instagram.com/ocean
Instagram::Ripper.reaper 'ocean'

# If you desire a html return type (defaults to JSON)
Instagram::Ripper.reaper :html, 'ocean'

# If you desire to dump the entire HTML page
Instagram::Ripper.reaper :json, true, 'ocean'
```
### Official docs

For detailed information, please visit the documentation page at RubyDoc: http://www.rubydoc.info/gems/instagram-ripper

## Instagram JSON

Basically, the gem will return a ``json`` object. Below it's explained a little about the JSON format.

### Extra information

JSON format:

```javascript
{images:
  [
    {image: {src: 'http://instagram.com/xyz/abc.jpg', alt: 'caption 1'}},
    {image: {src: 'http://instagram.com/xyz/123.jpg', alt: 'caption 2'}},
    ...
  ]
}
```

The *src* represents the location of the image and the *alt* represents the caption of the picture.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/rubygems/instagram-ripper.

